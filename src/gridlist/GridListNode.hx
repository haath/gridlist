package gridlist;

import gridlist.util.MathUtil;
import gridlist.util.ItemListHead;
import gridlist.GridListItem;
import gridlist.util.Direction.*;


@:generic
class GridListNode<T: GridListItem>
{
    public var cellHash(get, never): Int;
    var grid: GridList<T>;
    var x: Int;
    var y: Int;

    var itemsHead: ItemListHead;

    /** For linked list and pooling. **/
    @:allow(gridlist.GridList) var next: GridListNode<T>;
    @:allow(gridlist.GridList) var previous: GridListNode<T>;


    public inline function new(grid: GridList<T>, x: Int, y: Int)
    {
        this.grid = grid;
        itemsHead = new ItemListHead();

        reset(x, y);
    }


    public inline function reset(x: Int, y: Int)
    {
        this.x = x;
        this.y = y;
        itemsHead.__GridList_next = null;

        next = null;
        previous = null;
    }


    public inline function insert(item: T)
    {
        item.__GridList_prev = itemsHead;
        item.__GridList_next = itemsHead.__GridList_next;

        if (item.__GridList_next != null)
        {
            item.__GridList_next.__GridList_prev = item;
        }

        itemsHead.__GridList_next = item;
    }


    /**
     * Iterates over all items in the node.
     */
    public function forEach(callback: (item: T) -> Void)
    {
        var it: GridListItem =  itemsHead.__GridList_next;

        while (it != null)
        {
            callback(cast it);

            it = it.__GridList_next;
        }
    }


    public inline function isEmpty(): Bool
    {
        return (itemsHead.__GridList_next == null);
    }


    inline function get_cellHash(): Int
    {
        return MathUtil.hash(x, y);
    }
}
