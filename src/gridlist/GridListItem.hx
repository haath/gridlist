package gridlist;


@:autoBuild(gridlist.macro.GridListItemBuilder.build())
interface GridListItem
{
    @:noCompletion var __GridList_next: GridListItem;
    @:noCompletion var __GridList_prev: GridListItem;
}
