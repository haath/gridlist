package gridlist.macro;

import haxe.macro.Expr.Metadata;
#if macro
import haxe.macro.*;
import haxe.macro.Expr.Field;
import haxe.macro.Expr.ComplexType;
import haxe.macro.Expr.Position;
using haxe.macro.ExprTools;
using haxe.macro.TypeTools;
using haxe.macro.ComplexTypeTools;


class GridListItemBuilder
{
    static inline var nextVarName: String = "__GridList_next";
    static inline var prevVarName: String = "__GridList_prev";


    public static macro function build(): Array<Field>
    {
        var fields: Array<Field> = Context.getBuildFields();

        if (!isDirectImplementor())
        {
            // A parent class implements the GridListItem interface.
            return fields;
        }

        var ctype: ComplexType = Context.getLocalType().toComplexType();
        var meta: Metadata = [{ name: ':noCompletion', pos: pos }];


        // Add next pointer.
        fields.push({
            name: nextVarName,
            pos: pos,
            kind: FVar(macro: gridlist.GridListItem),
            access: [ APublic ],
            meta: meta
        });


        // Add previous pointer.
        fields.push({
            name: prevVarName,
            pos: pos,
            kind: FVar(macro: gridlist.GridListItem),
            access: [ APublic ],
            meta: meta
        });


        return fields;
    }


    static function isDirectImplementor(): Bool
    {
        for (intf in Context.getLocalClass().get().interfaces)
        {
            if (intf.t.get().name == "GridListItem")
            {
                return true;
            }
        }

        return false;
    }


    static var pos(get, never): Position;
    static function get_pos(): Position
    {
        return Context.currentPos();
    }
}

#end
