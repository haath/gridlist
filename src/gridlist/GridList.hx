package gridlist;

import gridlist.util.MathUtil;


@:generic
class GridList<T: GridListItem>
{
    var cells: GridListNode<T>;
    var cellLookup: Map<Int, GridListNode<T>>;
    var count: Int;

    var cellPool: GridListNode<T>;


    public function new()
    {
        cells = null;
        cellLookup = new Map();
        count = 0;

        cellPool = null;
    }


    /**
     * Inserts the given item to the grid list, at the cell with the specified coordinates.
     *
     * Calling this method while iterating through `forEach()` is not safe
     * and will have undefined effects.
     */
    public function insert(cellX: Int, cellY: Int, item: T)
    {
        if (!MathUtil.validCoordinates(cellX, cellY))
        {
            throw 'Coordinates need to be between -${MathUtil.CoordinateMax} and ${MathUtil.CoordinateMax}';
        }

        var hash: Int = MathUtil.hash(cellX, cellY);

        if (cellLookup.exists(hash))
        {
            cellLookup[hash].insert(item);
        }
        else
        {
            // Create the cell and insert the item.
            var cell: GridListNode<T> = createCell(cellX, cellY);
            cell.insert(item);

            // Add the cell to the grid list.
            cellLookup.set(hash, cell);
            cell.next = cells;
            cells = cell;

            if (cell.next != null)
            {
                cell.next.previous = cell;
            }
        }

        count++;
    }


    /**
     * Removes an item from the grid list.
     *
     * It is up to the user to ensure that the item exists in the list,
     * otherwise the behavior is undefined.
     *
     * Calling this method while iterating through `forEach()` is not safe
     * and will have undefined effects.
     */
    public inline function remove(item: T)
    {
        // Prev should never be null, it should be the list head.
        item.__GridList_prev.__GridList_next = item.__GridList_next;

        if (item.__GridList_next != null)
        {
            item.__GridList_next.__GridList_prev = item.__GridList_prev;
        }

        item.__GridList_prev = null;
        item.__GridList_next = null;

        count--;
    }


    /**
     * Iterates over all items in the list.
     *
     * The order in which cells and items are visited is undefined.
     *
     * @param callback The callback function to invoke for every item in the list.
     */
    public function forEach(callback: (item: T) -> Void)
    {
        var it: GridListNode<T> = cells;

        while (it != null)
        {
            var next: GridListNode<T> = it.next;

            if (it.isEmpty())
            {
                removeCell(it);
            }
            else
            {
                it.forEach(callback);
            }

            it = next;
        }
    }


    /**
     * Iterates over all items at a specific cell in the grid.
     *
     * @param x The x-coordinate of the cell to iterate.
     * @param y The y-coordinate of the cell to iterate.
     * @param callback The callback function to invoke for every item in the cell.
     */
    public inline function forEachAt(x: Int, y: Int, callback: (item: T) -> Void)
    {
        var cell: GridListNode<T> = cellLookup.get( MathUtil.hash(x, y) );

        if (cell != null)
        {
            cell.forEach(callback);
        }
    }


    /**
     * Iterates over all items in the list that are within a specified range from the given cell coordinates.
     *
     * For example, iterating around `(0, 0)` with a range of `1`, will go through all items in the cells
     * from `(-1, -1)` to `(+1, +1)`.
     *
     * @param x The x-coordinate of the central cell to iterate around.
     * @param y The y-coordinate of the central cell to iterate around.
     * @param range The range around `(x, y)` to iterate over.
     * @param callback The callback function to invoke for every item in the cells.
     */
    public function forEachInRange(x: Int, y: Int, range: Int, callback: (item: T) -> Void)
    {
        for (i in (x - range)...(x + range + 1))
        {
            for (j in (y - range)...(y + range + 1))
            {
                if (!MathUtil.validCoordinates(i, j))
                {
                    continue;
                }

                forEachAt(i, j, callback);
            }
        }
    }


    inline function removeCell(cell: GridListNode<T>)
    {
        cellLookup.remove(cell.cellHash);

        if (cell == cells)
        {
            cells = cell.next;
        }

        if (cell.previous != null)
        {
            cell.previous.next = cell.next;
        }
        if (cell.next != null)
        {
            cell.next.previous = cell.previous;
        }

        cell.next = null;
        cell.previous = null;
        destroyCell(cell);
    }


    /**
     * Returns the count of items currently in the grid list.
     */
    public inline function getCount(): Int
    {
        return count;
    }


    // =====================================================
    //                 POOLING METHODS
    // =====================================================

    inline function createCell(x: Int, y: Int): GridListNode<T>
    {
        if (cellPool == null)
        {
            return new GridListNode(this, x, y);
        }
        else
        {
            var cell: GridListNode<T> = cellPool;
            cellPool = cellPool.next;

            cell.reset(x, y);
            return cell;
        }
    }


    inline function destroyCell(cell: GridListNode<T>)
    {
        cell.next = cellPool;
        cellPool = cell;
    }
}
