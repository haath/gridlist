package gridlist.util;


class MathUtil
{
    /**
     * Hashes two integers `x` and `y` into a single integer.
     *
     * The integers must not be greater than `0x7FFF` or lesser than `-0x7FFF`.
     */
    public static inline function hash(x: Int, y: Int): Int
    {
        var signX: Int = (x >> 16) & (1 << 15);
        var signY: Int = y & (1 << 31);

        x = x & 0x7FFF;
        y = (y << 16) & 0x7FFF0000;

        return signY | y | signX | x;
    }


    /**
     * Returns the `x` component from a hash value obtained through `MathUtil.hash()`.
     */
    public static inline function getX(hash: Int): Int
    {
        if (hash & (1 << 15) != 0)
        {
            // Negative number.
            return 0xFFFF8000 | (hash & 0x7FFF);
        }
        else
        {
            // Positive number.
            return (hash & 0x7FFF);
        }
    }


    /**
     * Returns the `y` component from a hash value obtained through `MathUtil.hash()`.
     */
    public static inline function getY(hash: Int): Int
    {
        if (hash & (1 << 31) != 0)
        {
            // Negative number.
            return 0xFFFF8000 | ((hash >> 16) & 0x7FFF);
        }
        else
        {
            // Positive number.
            return ((hash >> 16) & 0x7FFF);
        }
    }


    public static inline var CoordinateMax = 0x7FFF;
    public static inline function validCoordinates(x: Int, y: Int): Bool
    {
        return x < CoordinateMax
            && x > -CoordinateMax
            && y < CoordinateMax
            && y > -CoordinateMax;
    }
}
