package gridlist.util;


@:enum
abstract Direction(Int) to Int
{
    var TopLeft;
    var Top;
    var TopRight;
    var Left;
    var Right;
    var BottomLeft;
    var Bottom;
    var BottomRight;


    public inline function dirX(): Int
    {
        return switch this
        {
            case TopRight | Right | BottomRight: 1;
            case TopLeft | Left | BottomLeft: -1;
            case _: 0;
        }
    }


    public inline function dirY(): Int
    {
        return switch this
        {
            case TopLeft | Top | TopRight: 1;
            case BottomLeft | Bottom | BottomRight: -1;
            case _: 0;
        }
    }
}
