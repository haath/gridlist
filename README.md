# gridlist

Spatial partitioning 2D list in the form of a linked grid, with a focus on iteration speed. It functions much like a 2-dimensional array, but without statically allocating any space.

- Underlying linked lists, minimal memory allocations.
- Insertion and removal in `O(1)`.
- Iteration with no allocations.
- Iteration over neighbor items in `O(1)`.


### Usage

Types to be put in the list should implement `GridListItem`.

```haxe
class Item implements GridListItem
{
    // ...
}
```

```haxe
var grid: GridList<Item> = new GridList<Item>();

// Insert an item in the cell (0, 0)
grid.insert(0, 0, item);

// Insert an item in the cell (0, 1)
grid.insert(0, 1, item2);

// Remove an item
grid.remove(item3);
```

Then to iterate over the list, use the `forEach()` and `forEachInRange()` methods.

```haxe
// Iterate over all items.
grid.forEach(item ->
{
    // ...
});


// Iterate over items at (1, 1) and in a 2-cell range around (1, 1).
grid.forEachInRange(1, 1, 2, item ->
{
    // ...
});
```

