package types;

import gridlist.GridListItem;


class Item implements GridListItem
{
    public var value(default, null): Int;


    public function new(value: Int)
    {
        this.value = value;
    }
}
