package tests;

import gridlist.util.MathUtil;
import gridlist.GridListNode;
import utest.Assert;
import utest.ITest;
import gridlist.GridList;
import types.*;


class MathUtilTest implements ITest
{
    public function new() { }


    function testHash()
    {
        var testRange: Int = 200;
        var existing: Map<Int, String> = new Map();

        for (x in -testRange...testRange)
        {
            for (y in -testRange...testRange)
            {
                var hash: Int = MathUtil.hash(x, y);

                var exists: Bool = existing.exists(hash);
                Assert.isFalse( existing.exists(hash), '($x, $y) -> $hash exists from ${existing[hash]}' );

                Assert.equals(x, MathUtil.getX(hash));
                Assert.equals(y, MathUtil.getY(hash));

                if (exists || (x != MathUtil.getX(hash))) return;

                existing.set(hash, '($x, $y)');
            }
        }
    }
}
