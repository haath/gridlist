package tests;

import seedyrng.Seedy;
import gridlist.util.MathUtil.hash;
import gridlist.util.MathUtil;
import haxe.CallStack;
import gridlist.GridListNode;
import utest.Assert;
import utest.ITest;
import gridlist.GridList;
import types.*;


@:access(gridlist.GridList)
@:access(gridlist.GridListNode)
class GridListTest implements ITest
{
    var gridList: GridList<Item>;


    public function new() { }


    function setup()
    {
        gridList = new GridList();
    }


    function testCreateCell()
    {
        var node1: GridListNode<Item> = gridList.createCell(1, 2);
        var node2: GridListNode<Item> = gridList.createCell(5, 5);

        Assert.notEquals(node1, node2);
        Assert.equals(1, node1.x);
        Assert.equals(2, node1.y);
        Assert.equals(null, node1.next);
        Assert.equals(5, node2.x);
        Assert.equals(5, node2.y);
        Assert.equals(null, node2.next);
    }


    function testDestroyCell()
    {
        var node1: GridListNode<Item> = gridList.createCell(1, 2);
        var node2: GridListNode<Item> = gridList.createCell(5, 5);
        gridList.destroyCell(node1);

        var node3: GridListNode<Item> = gridList.createCell(6, 6);
        Assert.notEquals(node1, node2);
        Assert.equals(node1, node3);
        Assert.equals(6, node3.x);
        Assert.equals(6, node3.y);
        Assert.equals(null, node3.next);
    }


    function testInsert()
    {
        insert(0, 0, 1);
        insert(0, 0, 2);
        insert(1, 5, 3);

        assertList([1, 2, 3]);
    }


    function testInsertInvalidCoordinates()
    {
        Assert.raises( () -> gridList.insert(-0xFFFF, 0, new Item(1)), String );
    }


    function testRemoveCell()
    {
        var i1: Item = insert(0, 0, 1);
        assertList([1]);
        var i21: Item = insert(0, 1, 2);
        assertList([1, 2]);

        var cell00: GridListNode<Item> = gridList.cellLookup[hash(0, 0)];
        var cell01: GridListNode<Item> = gridList.cellLookup[hash(0, 1)];

        Assert.equals(cell01, gridList.cells);
        Assert.equals(cell00, gridList.cells.next);

        gridList.removeCell(cell00);

        Assert.equals(cell01, gridList.cells);
        Assert.equals(null, gridList.cells.next);
    }


    function testRemove()
    {
        var i1: Item = insert(0, 0, 1);
        var i2: Item = insert(0, 0, 2);
        var i3: Item = insert(0, 0, 3);
        var i4: Item = insert(0, 0, 4);
        var i5: Item = insert(0, 0, 5);
        var i6: Item = insert(1, 5, 6);

        assertListInRange(0, 0, [1, 2, 3, 4, 5]);

        gridList.remove(i1);
        assertList([2, 3, 4, 5, 6]);
        assertNoEmptyCells();
        Assert.equals(2, getActiveCellCount());

        gridList.remove(i3);
        assertList([2, 4, 5, 6]);
        assertNoEmptyCells();
        Assert.equals(2, getActiveCellCount());

        gridList.remove(i4);
        assertList([2, 5, 6]);
        assertNoEmptyCells();
        Assert.equals(2, getActiveCellCount());

        gridList.remove(i6);
        assertList([2, 5]);
        assertNoEmptyCells();
        Assert.equals(1, getActiveCellCount());

        var i7: Item = insert(1, 5, 7);
        assertList([2, 5, 7]);
        assertNoEmptyCells();
        Assert.equals(2, getActiveCellCount());

        var i8: Item = insert(2, 5, 8);
        assertList([2, 5, 7, 8]);
        assertNoEmptyCells();
        Assert.equals(3, getActiveCellCount());

        gridList.remove(i5);
        assertList([2, 7, 8]);
        assertNoEmptyCells();
        Assert.equals(3, getActiveCellCount());

        gridList.remove(i2);
        assertList([7, 8]);
        assertNoEmptyCells();
        Assert.equals(2, getActiveCellCount());

        gridList.remove(i8);
        assertList([7]);
        assertNoEmptyCells();
        Assert.equals(1, getActiveCellCount());

        gridList.remove(i7);
        assertList([]);
        assertNoEmptyCells();
        Assert.equals(0, getActiveCellCount());
    }


    function testLargeRandom()
    {
        var count: Int = 1000;
        var values: Array<Item> = [for (i in 0...count) new Item(i)];

        Seedy.shuffle(values);

        var inserted: Array<Item> = [];

        for (i in 0...values.length)
        {
            var item: Item = values[i];

            var cellX: Int = Seedy.randomInt(-10, 10);
            var cellY: Int = Seedy.randomInt(-10, 10);

            gridList.insert(cellX, cellY, item);
            inserted.push(item);

            // Randomly remove one item.
            if (Seedy.random() < 0.3)
            {
                var toRemove: Item = Seedy.choice(inserted);
                gridList.remove(toRemove);
                inserted.remove(toRemove);
            }

            assertListItems(inserted.copy());
            assertNoEmptyCells();
        }

        // Remove all remaining items.
        Seedy.shuffle(inserted);
        while (inserted.length > 0)
        {
            var toRemove: Item = inserted.pop();
            gridList.remove(toRemove);

            assertListItems(inserted.copy());
            assertNoEmptyCells();
        }

        // Grid list should now be empty.
        Assert.isNull(gridList.cells);
        Assert.equals(0, Lambda.count(gridList.cellLookup));
    }


    function assertList(lst: Array<Int>)
    {
        Assert.equals(lst.length, gridList.getCount());

        gridList.forEach(item ->
        {
            Assert.isTrue( lst.remove(item.value), 'unexpected item: ${item.value}\n' );
        });

        Assert.equals(0, lst.length);
    }


    function assertListItems(lst: Array<Item>)
    {
        Assert.equals(lst.length, gridList.getCount());

        gridList.forEach(item ->
        {
            Assert.isTrue( lst.remove(item), 'unexpected item: ${item.value}\n' );
        });

        Assert.equals(0, lst.length);
    }


    function assertListInRange(x: Int, y: Int, lst: Array<Int>)
    {
        gridList.forEachInRange(x, y, 1, item ->
        {
            Assert.isTrue( lst.remove(item.value), 'unexpected item: ${item.value}\n' );
        });

        Assert.equals(0, lst.length);
    }


    function insert(x: Int, y: Int, value: Int): Item
    {
        var item: Item = new Item(value);
        gridList.insert(x, y, item);
        return item;
    }


    function assertNoEmptyCells()
    {
        // Check linked list.
        var it: GridListNode<Item> = gridList.cells;
        while (it != null)
        {
            Assert.isFalse( it.isEmpty(), 'empty cell: (${it.x}, ${it.y})' );
            it = it.next;
        }

        // Check lookup table.
        for (hash => it in gridList.cellLookup)
        {
            Assert.isFalse( it.isEmpty(), 'empty cell: (${it.x}, ${it.y})' );
        }
    }


    function getActiveCellCount(): Int
    {
        var count: Int = 0;
        var it: GridListNode<Item> = gridList.cells;
        while (it != null)
        {
            if (!it.isEmpty())
            {
                count++;
            }
            it = it.next;
        }
        return count;
    }
}
